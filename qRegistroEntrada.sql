

----------------------------------
-- vRegistroEntrada
-- Autor: Omar
-- Fecha:25-03-2022
-- Cambios: inicial
----------------------------------

CREATE VIEW vRegistroEntrada
AS 
SELECT d.apellidoPaterno as ApeliidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,m.municipio as MunicipioDetencion
		,e.estado as EstadoDetencion
		,dt.patrulla
		,o1.apellidoPaterno as ApellidoPaternoOficial1
		,o1.apellidoMaterno as ApellidoMaternoOficial1
		,o1.nombre as NombreOficial1
		,o2.apellidoPaterno as ApellidoPaternoOficial2
		,o2.apellidoMaterno as ApellidoMaternoOficial2
		,o2.nombre as NombreOficial2
		,d.edad as EdadDetenido
		,s.sexo as SexoDetenido
		,oc.ocupacion as OcupacionDetenido
		,ec.estadoCivil as EstadoCivilDtenido
		,eo.estado as EstadoOrigenDetenido
		,es.escolaridad as EscolaridadDetenido
		,dt.calle1Delito
		,dt.calle2Delito
		,cd.colonia as ColoniaDetencion ---Preguntar porque debe ser coloniadetencion
		,d.calle as CalleDomicilioDetenido
		,md.municipio as MunicipioDomicilioDetenido
		,cdm.colonia as ColoniaDomicilioDetenido
		,d.numeroTelefonico as TelefonoDetenido
		,dt.horaEntrada as HoraEntrada
		,dt.fechaEntrada as FechaEntrada
		,dt.id as Remision
		,dt.observaciones 
		,j.apellidoPaterno as ApellidoPaternoJuez
		,j.apellidoMaterno as ApellidoMaternoJuez
		,j.nombre as NombreJuez

FROM Detenido d
LEFT JOIN Detencion dt
ON dt.idDetenido = d.id
LEFT JOIN Municipio m
ON m.id = dt.idMunicipioDetencion
LEFT JOIN Estado e
ON e.id = m.idestado
LEFT JOIN Oficiales o1
ON o1.id=dt.idPolicia1
LEFT JOIN Oficiales o2
ON o2.id = dt.idPolicia2
LEFT JOIN Sexo s
ON s.id = d.idsexo
LEFT JOIN Ocupacion oc
ON oc.id = d.idocupacion
LEFT JOIN EstadoCivil ec
ON ec.id = d.idestadoCivil
LEFT JOIN Municipio mo
ON mo.id = d.idmunicipio
LEFT JOIN Estado eo
ON eo.id = mo.idestado
LEFT JOIN Escolaridad es
ON es.id =  d.idescolaridad
LEFT JOIN Colonia cd
ON cd.id = dt.idColoniaDelito
LEFT JOIN Municipio md
ON md.id = d.municipio
LEFT JOIN Colonia cdm
ON cdm.id = d.idcolonia
LEFT JOIN Jueces j
ON j.id = dt.idJuez
;