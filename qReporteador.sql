
----------------------------------
-- vReporteador
-- Autor: Omar
-- Fecha:25-03-2022
-- Cambios: inicial
----------------------------------
CREATE VIEW vReporteador
AS
SELECT d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,'' as Remision
		,dt.horaEntrada
		,dt.fechaEntrada
		,m.municipio as MunicipioDetenido
FROM Detenido d
LEFT JOIN Detencion dt
ON dt.idDetenido = dt.id
LEFT JOIN Municipio m
ON m.id = dt.idMunicipioDetencion
;