
----------------------------------
-- vAliasRegistroEntradaDelito
-- Autor: Omar
-- Fecha:25-03-2022
-- Cambios: inicial
----------------------------------

CREATE VIEW vAliasRegistroEntradaDelito
AS
SELECT d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,dt.alias
FROM Detenido D
LEFT JOIN Detencion dt
ON dt.idDetenido = d.id
;