

----------------------------------
-- vFaltaAdministrativaRegistroEntrada
-- Autor: Omar
-- Fecha:25-03-2022
-- Cambios: inicial
----------------------------------
CREATE VIEW vFaltaAdministrativaRegistroEntrada
AS
SELECT d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,fa.falta
FROM Detenido d
LEFT JOIN Detencion dt
ON dt.idDetenido = d.id
LEFT JOIN FaltaAdministrativa fa
ON fa.id = dt.idfaltaAdministrativa
;