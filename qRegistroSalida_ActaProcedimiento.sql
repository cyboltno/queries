
----------------------------------
-- vRegistroSalidaActaProcedimiento
-- Autor: Omar
-- Fecha:22-03-2022
-- Cambios: inicial
----------------------------------
CREATE VIEW vRegistroSalidaActaProcedimiento
AS
SELECT d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApeliidoMaternoDetenido
		,d.nombre as NombreDetenido
		,m.municipio as MunicipioDetenido
		,e.estado as EstadoDetenido
		,'' as Destino
		,'' as leyenda
		,'' as fechaSalida
		,'' as HoraSalida
		,j.apellidoPaterno as ApeliidoPaternoJuez
		,j.apellidoMaterno as ApellidoMaternoJuez
		,j.nombre as NombreJuez
		,j.cedula as CedulaJuez
FROM Detenido d
INNER JOIN Detencion dt
ON dt.idDetenido = d.id
LEFT JOIN Municipio m
ON dt.idMunicipioDetencion = m.id
LEFT JOIN Estado e
ON m.idestado = e.id
LEFT JOIN Jueces j
ON j.id = dt.idJuez
;