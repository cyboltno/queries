


----------------------------------
-- vDelitoRegistroEntradaDelito
-- Autor: Omar
-- Fecha:25-03-2022
-- Cambios: inicial
----------------------------------
CREATE VIEW vDelitoRegistroEntradaDelito
AS
SELECT d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,dt.alias
		,o1.apellidoPaterno as ApellidoPaternoOficial1
		,o1.apellidoPaterno as ApellidoMaternoOficial1
		,o1.nombre as NombreOficial1
		,o2.apellidoPaterno as ApellidoPaternoOficial2
		,o2.apellidoMaterno as ApellidoMaternoOficial2
		,o2.nombre as NombreOficial2
		,dt.tipoDelito
FROM Detenido D
LEFT JOIN Detencion dt
ON dt.idDetenido = d.id
LEFT JOIN Oficiales o1
ON dt.idPolicia1 = o1.id
LEFT JOIN Oficiales o2
ON dt.idPolicia2 = o2.id
;