

----------------------------------
-- vEvalucacionPsicosocialDelRiesgo
-- Autor: Omar
-- Fecha:24-03-2022
-- Cambios: inicial
----------------------------------

CREATE VIEW vEvalucacionPsicosocialDelRiesgo
AS
SELECT m.municipio
		,e.estado
		,ep.fechaCreacion as FechaImpresion
		,ev.apellidoPaterno as ApellidoPaternoEvaluador
		,ev.apellidoMaterno as ApellidoMaternoEvaluador
		,ev.nombre as NombreEvaluador
		,d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,'' as FolioPH
		,d.edad as EdadDetenido
		,s.sexo as SexoDetenido
		,d.originario as Nacionalidad
		,oc.ocupacion
		,ec.estadoCivil EstadoCivilDetenido
		,mo.municipio as MunicipioDomicilio
		,eo.estado as EstadoDomicilio
		,es.escolaridad as EscolaridadDetenido
		,d.calle as CalleDomicilio
		,cl.colonia as ColoniaDomicilio
		,dt.correoElectronicoContacto
		,dt.telefonoContacto
		,fa.falta as FaltaAdministrativa
		,'' as NumeroDetencionesEnElAņo

		

FROM EvaluacionPsicosocial ep
LEFT JOIN Municipio m
ON m.id = ep.idMunicipio
LEFT JOIN Estado e
ON e.id = m.idestado
LEFT JOIN Evaluador ev
ON ev.id = ep.idEvaluador
LEFT JOIN Detenido d
ON d.id = ep.idDetenido
LEFT JOIN Sexo s
ON d.idsexo = s.id
LEFT JOIN Ocupacion oc
ON oc.id = d.idocupacion
LEFT JOIN EstadoCivil ec
ON ec.id = d.idestadoCivil
LEFT JOIN Municipio mo
ON mo.id = d.idmunicipio
LEFT JOIN Estado eo
ON eo.id = mo.idestado
LEFT JOIN Escolaridad es
ON es.id = d.idescolaridad
LEFT JOIN Colonia cl
ON cl.id = d.idcolonia
LEFT JOIN Detencion dt
ON dt.idDetenido = d.id
LEFT JOIN FaltaAdministrativa fa
ON fa.id = dt.idfaltaAdministrativa
;