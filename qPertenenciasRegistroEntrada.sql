
----------------------------------
-- vPertenenciasRegistroEntrada
-- Autor: Omar
-- Fecha:25-03-2022
-- Cambios: inicial
----------------------------------
CREATE VIEW vPertenenciasRegistroEntrada
AS
SELECT d.apellidoMaterno as ApellidoPaternoDetenido
		,d.apellidoPaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,p.pertenencia as PertenenciaDetenido
FROM Detenido d
LEFT JOIN Detencion dt
ON dt.idDetenido = d.id
LEFT JOIN pertenencias_detencion pd
ON dt.id = pd.iddetencion
LEFT JOIN Pertenencias p
ON pd.idpertenencia = p.id

;