

----------------------------------
-- vAsignacion
-- Autor: Omar
-- Fecha:23-03-2022
-- Cambios: inicial
----------------------------------
CREATE VIEW vAsignacion
AS
SELECT d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,m.municipio as MunicipioDetencion
		,e.estado as EstadoDetencion
		,'' as Responsable
		,'' as FechaAsignacionSC
		,dt.id as remision
		,'' as horasDelSC
FROM Detenido d
INNER JOIN Detencion dt
ON dt.idDetenido = d.id
LEFT JOIN Municipio m
ON m.id = dt.idMunicipioDetencion
LEFT JOIN Estado e
ON e.id = m.idestado
;