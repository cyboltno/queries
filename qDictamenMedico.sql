

----------------------------------
-- vDictamenMedicoAlcholimetro
-- Autor: Omar
-- Fecha:24-03-2022
-- Cambios: inicial
----------------------------------
CREATE VIEW vDictamenMedicoAlcholimetro

AS SELECT d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,'' as Clave
		,'' as FolioDictamen
		,m.municipio as MunicipioDetencion
		,e.estado as EstadoDetencion
		,dt.horaEntrada as HoraEntrada
		,dt.fechaEntrada as FechaEntrada
		,md.municipio as MunicipioDictamen
		,ed.estado as EstadoDictamen
		,o.apellidoPaterno as ApellidoPaternoOficial
		,o.apellidoMaterno as ApellidoMaternoOficial
		,o.nombre as NombreOficial
		,tof.tipo as TipoOficial
		,mof.municipio as MunicipioOficial
		,o.numPlaca as NumeroPlaca
		,s.sexo as SexoDetenido
		,d.edad as EdadDetenido
		,ec.estadoCivil as EstadoCivilDetenido
		,oc.ocupacion as OcupacionDetenido
		,d.calle as CalleDomicilioDetenido
		,mo.municipio as MunicipioOrigen
		,eo.estado as EstadoOrigen
		,tpa.tipo as TipoPruebaAlcohol
		,rp.resultado as ResultadoPruebaAlcohol
		,ca.porcentaje as GramosLitro
		,dm.numeroSerie 
		,dm.fechaCalibracionAlcoholimetro
		,dm.pruebaNumAlcoholimetro
		,dm.horaPruebaAlcoholimetro
		,dm.fechaPruebaAlcoholimetro
		,dm.presentaLesiones
		,dm.lesionQuinceDias as LesionMas15Dias
		,dm.lesionPeligrosa
		,dm.quedaraCicatriz
		,dm.observaciones
		,dm.horaFinalDictamen
		,dm.fechaFinalDictamen
		,dm.nombreOtroMedico as Medico
		,dm.cedulaOtroMedico as CedulaMedico

FROM DictamenMedico dm
LEFT JOIN Detenido d 
ON dm.idDetenido = d.id
LEFT JOIN Detencion dt
ON dt.idDetenido = d.id
LEFT JOIN Municipio m
ON dt.idMunicipioDetencion = m.id
LEFT JOIN Estado e
ON m.idestado = e.id
LEFT JOIN Oficiales o
ON o.id = dm.idOficial
LEFT JOIN TipoOficial tof
ON o.idTipoOficial = tof.id
LEFT JOIN Municipio mof
ON mof.id = o.idmunicipio
LEFT JOIN Municipio md
ON md.id = dm.idMunicipioDictamen
LEFT JOIN Estado ed
ON ed.id = md.idestado
LEFT JOIN Sexo s
ON s.id = d.idsexo
LEFT JOIN EstadoCivil ec
ON ec.id = d.idestadoCivil
LEFT JOIN Ocupacion oc
ON oc.id = d.idocupacion
LEFT JOIN Municipio mo
ON mo.id = d.idmunicipio
LEFT JOIN Estado eo
ON eo.id = mo.idestado
LEFT JOIN TipoPruebasAlcohol tpa
ON tpa.id = dm.idTipoPrueba
LEFT JOIN ResultadoPercepcion rp
ON rp.id = dm.idResultadoPercepcion
LEFT JOIN ConcentracionAlcohol ca
ON dm.idConcentracionAlcohol = ca.id

;