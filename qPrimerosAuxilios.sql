

----------------------------------
-- vPrimeroAuxilios
--Tambien Funciona para multa2
-- Autor: Omar
-- Fecha:24-03-2022
-- Cambios: inicial
----------------------------------

CREATE VIEW vPrimeroAuxilios
AS
SELECT d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as Nombre
		,pa.tipoAuxilio as TipoAxilio
		,m.municipio as MunicipioPrimerosAxilios
		
FROM Detenido d
LEFT JOIN Detencion dt
ON dt.idDetenido = d.id
LEFT JOIN PrimerosAuxilios pa
ON pa.idDetenido =d.id
LEFT JOIN Municipio m
ON m.id = pa.idMunicipio

;