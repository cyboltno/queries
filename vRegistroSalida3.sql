----------------------------------
-- vRegistroSalida3
-- Autor: Omar
-- Fecha:25-03-2022
-- Cambios: inicial
----------------------------------

CREATE VIEW vRegistroSalida3
AS
SELECT d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApeliidoMaternoDetenido
		,d.nombre as NombreDetenido
		,m.municipio as MunicipioDetencion
		,dt.id as Remision
		,'' as Destino
		,'' as HoraSalida
		,'' as FechaSalida
		,j.apellidoPaterno as ApellidoPaternoJuez
		,j.apellidoMaterno as ApellidoMaternoJuez
		,j.nombre as NombreJuez
		,j.cedula as CedulaJuez
		,jc.multa as Multa
		,'' as Dictamen_precio_creo
		,'' as MultaTotal
		,o.apellidoPaterno as ApellidoPaternoGuardia
		,o.apellidoMaterno as ApellidoMaternoGuardia
		,o.nombre as NombreGuardia
FROM Detenido d
LEFT JOIN Detencion dt
ON dt.idDetenido = d.id
LEFT JOIN Municipio m
ON m.id = dt.idMunicipioDetencion
LEFT JOIN Jueces j
ON j.id = dt.idJuez
LEFT JOIN Juicio jc
ON jc.idDetencion = dt.id
LEFT JOIN Oficiales o
ON o.id = dt.idOficialGuardia
;