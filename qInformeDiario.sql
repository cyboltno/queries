
----------------------------------
-- vInformeDiario
-- Autor: Omar
-- Fecha:24-03-2022
-- Cambios: inicial
----------------------------------

CREATE VIEW vInformeDiario
AS
SELECT dt.id as Remision
		,d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,dt.horaEntrada
		,dt.fechaEntrada
		,m.municipio as MunicipioDetencion
FROM Detenido d
LEFT JOIN Detencion dt
ON dt.idDetenido = d.id
LEFT JOIN Municipio m
ON dt.idMunicipioDetencion = m.id
;