

----------------------------------
-- vReporteDiarioSecretario
-- Autor: Omar
-- Fecha:25-03-2022
-- Cambios: inicial
----------------------------------

CREATE VIEW vReporteDiarioSecretario
AS
SELECT '' as HoraUno
		,'' as FechaInicial
		,'' as HoraDos
		,'' as FechaFinal
		,'' as FechaDeLosDatos
FROM Detenido d
;