

----------------------------------
-- vReporteDeAsistencias
-- Autor: Omar
-- Fecha:25-03-2022
-- Cambios: inicial
----------------------------------

CREATE VIEW vReporteDeAsistencias
AS
SELECT d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,d.edad as EdadDetenido
		,d.calle as CalleDetenido
		,m.municipio as MunicipioDomicilioDetenido
		,c.colonia
		,s.horaSancion as TiempoAcumplir
		,'' as HorasAcomuladas
FROM Detenido d
LEFT JOIN Detencion dt
ON d.id = dt.idDetenido
LEFT JOIN Municipio m
ON m.id = d.idmunicipio
LEFT JOIN Colonia c
ON c.id = d.idcolonia
LEFT JOIN Juicio j
ON j.idDetencion = dt.id
LEFT JOIN Sancion s
ON s.idDetenido = d.id
;