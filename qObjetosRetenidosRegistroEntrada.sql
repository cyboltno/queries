

----------------------------------
-- vObjetosRetenidosRegistroEntrada
-- Autor: Omar
-- Fecha:25-03-2022
-- Cambios: inicial
----------------------------------

CREATE VIEW vObjetosRetenidosRegistroEntrada
AS
SELECT d.apellidoMaterno as ApellidoPaternoDetenido
		,d.apellidoPaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,o.objeto
FROM Detenido d
LEFT JOIN Detencion dt
ON dt.idDetenido = d.id
LEFT JOIN objetos_detencion od
ON dt.id = od.iddetencion
LEFT JOIN Objetos o
ON o.id = od.idobjeto


;