
----------------------------------
-- vQuejosoRegistroEntrada
-- Autor: Omar
-- Fecha:25-03-2022
-- Cambios: inicial
----------------------------------
CREATE VIEW vQuejosoRegistroEntrada
AS
SELECT qd.apellidoPaterno as ApellidoPaternoParteQuejante
		,qd.apellidoMaterno as ApellidMaternoParteQuejante
		,qd.nombre as NombreParteQuejante
		,qd.edad as EdadParteQuejante
		,oc.ocupacion as OcupacionParteQuejante
		,c.colonia as ColoniaParteQuejante
		,m.municipio as MunicipioParteQuejante
FROM QuejosoDetencion qd
LEFT JOIN Ocupacion oc
ON oc.id = qd.idocupacion
LEFT JOIN Municipio m
ON m.id = qd.idmunicipio
LEFT JOIN Colonia c
ON c.id = qd.idcolonia
;