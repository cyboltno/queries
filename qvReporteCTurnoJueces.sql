

----------------------------------
-- vReporteCTurnoJueces
-- Autor: Omar
-- Fecha:25-03-2022
-- Cambios: inicial
----------------------------------

CREATE VIEW vReporteCTurnoJueces
AS
SELECT j.apellidoPaterno as ApellidoPaternoJuez
		,j.apellidoMaterno as ApellidoMaternoJuez
		,j.nombre as NombreJuez
FROM Jueces j
;