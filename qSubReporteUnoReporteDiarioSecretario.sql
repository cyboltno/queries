

----------------------------------
-- vSubReporteUnoReporteDiarioSecretario
-- Autor: Omar
-- Fecha:25-03-2022
-- Cambios: inicial
----------------------------------
CREATE VIEW vSubReporteUnoReporteDiarioSecretario
AS
SELECT dt.id as Remision
		,d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,d.edad as EdadDetenido
		,'' as STATUS
		,m.municipio as MunicipioDetencion
FROM Detenido d
LEFT JOIN Detencion dt
ON dt.idDetenido = d.id
LEFT JOIN Municipio m
ON m.id = dt.idMunicipioDetencion
;