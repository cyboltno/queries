
----------------------------------
-- vComparecencia
-- Autor: Omar
-- Fecha:23-03-2022
-- Cambios: inicial
----------------------------------

CREATE VIEW vComparecencia
AS
SELECT d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,m.municipio as MunicipioDetencion
		,e.estado as EstadoDetencion
		,'' as HoraDeExpedicion
		,'' as FechaExpedicion
		,'' as NombreJuez
		,d.edad
		,'' as RFC
		,o.ocupacion
		,es.escolaridad
		,ec.estadoCivil
		,d.numeroTelefonico
		,'' as email
		,d.calle as CalleDomicilo
		,c.colonia as ColoniaDomicilio
		,m2.municipio as MunicipioDomiciol
		,dt.id as remision
		,'' as reciboTesoreria
		,'' as multaTotalComparecencia
		,'' as horasDelSC
FROM Detenido d
INNER JOIN Detencion dt
ON dt.idDetenido = d.id
LEFT JOIN Municipio m
ON m.id = dt.idMunicipioDetencion
LEFT JOIN Estado e
ON e.id = m.idestado
LEFT JOIN Ocupacion o
ON o.id = d.idocupacion
LEFT JOIN Escolaridad es
ON es.id = d.idescolaridad
LEFT JOIN EstadoCivil ec
ON ec.id = d.idestadoCivil
LEFT JOIN Colonia c
ON c.id = d.idcolonia
LEFT JOIN Municipio m2
ON m2.id = c.idmunicipio
;