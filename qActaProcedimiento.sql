
----------------------------------
-- vActaProcedimiento
-- Autor: Omar
-- Fecha:22-03-2022
-- Cambios: inicial
----------------------------------
CREATE VIEW vActaProcedimiento
AS
SELECT d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,dt.idMunicipioDetencion
		,m.municipio as MunicipioDetencion
		,e.estado as EstadoDetencion
		,j.fechaJuicio as fechaAudencia
		,s.sexo as Sexo
		,dt.tipoDelito
		,dt.fechaDetencion
		,dt.horaDetencion
		,dt.calle1Delito
		,dt.calle2Delito
		,c.colonia -- Debe ser coloniaDetencion
		,j.circunstancias
		,'' as NuevoTotal
		,'' as CostoDictamen
		,j.multa as multa
		,j.multaUmas as SalariosMinimos
		,j.horasArresto as HorasArresto
		,rp.resultado as ResultadoDictamenMedico
		,d.ocupacion
		,j.cumpleArresto as cumpleArresto
		,dt.realizoLlamada
		,tcl.tipo as PersonaAQuienSeLlamo
		,j.fechaJuicio
		,j.horaJuicio
		,'' as Procedimiento
		,dt.id as Remision
		,mns.motivo as MotivoNoSancion
		,ju.apellidoPaterno as ApellidoPaternoJuez
		,ju.apellidoMaterno as ApellidoMaternoJuez
		,ju.nombre as NombreJuez
		,o1.apellidoPaterno as ApellidoPaternoOficial1
		,o1.apellidoMaterno as ApeliidoMaternoOficial1
		,o1.nombre as NombreOficial1
		,o2.apellidoPaterno as ApellidoPaternoOficial2
		,o2.apellidoMaterno as ApellidoMaternoOficial2
		,o2.nombre as NombreOficial2
		,o3.apellidoPaterno as ApellidoPaternoGuardia
		,o3.apellidoMaterno as ApellidoMaternoGuardia
		,o3.nombre as NombreGuardia

FROM Detenido d
LEFT JOIN Detencion dt 
On dt.idDetenido = d.id
LEFT JOIN Municipio m
ON dt.idMunicipioDetencion = m.id
LEFT JOIN Estado e
ON e.id = m.idestado
LEFT JOIN Colonia c
ON c.id = dt.idColoniaDelito
LEFT JOIN Juicio j
ON j.idDetencion = dt.id
LEFT JOIN DictamenMedico dm
ON dm.idDetenido = d.id
LEFT JOIN ResultadoPercepcion rp
ON rp.id = dm.idResultadoPercepcion
LEFT JOIN TipoContactoLlamada tcl
ON tcl.id = dt.idPersonaLlamada
LEFT JOIN Sexo s
ON s.id = d.idsexo
LEFT JOIN MotivosNoSancion mns
ON mns.id = j.idMotivoNoSancion
LEFT JOIN Jueces ju
ON ju.id = dt.idJuez
LEFT JOIN Oficiales o1
ON o1.id = dt.idPolicia1
LEFT JOIN Oficiales o2
ON o1.id = dt.idPolicia2
LEFT JOIN Oficiales o3
ON o3.id = dt.idOficialGuardia
;

