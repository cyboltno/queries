


----------------------------------
-- vAsistenciasReporteDeAsistencias
-- Autor: Omar
-- Fecha:25-03-2022
-- Cambios: inicial
----------------------------------
CREATE VIEW vAsistenciasReporteDeAsistencias
AS
SELECT d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,'' as Numero
		,'' as FechaLlegada
		,'' as HoraLlegada
		,'' as FechaSalida
		,'' as HoraSalida
		,'' as TiempoTotal
FROM Detenido d

;