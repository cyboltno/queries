

----------------------------------
-- vQueja
-- Autor: Omar
-- Fecha:24-03-2022
-- Cambios: inicial
----------------------------------

CREATE VIEW vQueja
AS
SELECT q.apellidoPaterno as ApellidoPaternoDenunciante
		,q.apellidoMaterno as ApellidoMaternoDenunciante
		,q.nombre as NombreDenunciante
		,m.municipio as MunicipioQueja
		,q.fechaCreacion as FechaCreacion
		,'' as HoraDeCreacion
		,j.apellidoPaterno as ApellidoPaternoJuez
		,j.apellidoMaterno as ApellidoMaternoJuez
		,j.nombre as NombreJuez
		,q.domicilio as DomicilioDenunciante
		,cl.colonia as ColoniaDomicilioDenunciante
		,mq.municipio as MunicipioDomicilioDenunciante
		,q.telefono as TelefonoDenunciante
		,q.email as emailDenunciante
		,q.paternoInfractor as ApellidoPaternoInfractor
		,q.maternoInfractor as ApellidoMaternoInfractor
		,q.nombreInfractor as NombreInfractor
		,cli.colonia as ColoniaInfractor
		,mi.municipio as MunicipioInfractor

FROM Queja q
LEFT JOIN Municipio m
ON m.id = q.idMunicipioQueja
LEFT JOIN Jueces j
ON j.id = q.idJuez
LEFT JOIN Colonia cl
ON cl.id = q.idColoniaQuejoso
LEFT JOIN Municipio mq
on mq.id = q.idMunicipioQuejoso
LEFT JOIN Colonia cli
ON cli.id = q.idColoniaInfractor
LEFT JOIN Municipio mi
ON mi.id = q.idMunicipioInfractor
;