

----------------------------------
-- vMulta
--Tambien Funciona para multa2
-- Autor: Omar
-- Fecha:24-03-2022
-- Cambios: inicial
----------------------------------
CREATE VIEW vMulta
AS
SELECT d.apellidoPaterno as ApellidoPaternoDetenido
		,d.apellidoMaterno as ApellidoMaternoDetenido
		,d.nombre as NombreDetenido
		,m.municipio as MunicipioDetencion
		,e.estado as EstadoDetencion
		,d.calle as CalleDomicilio
		,cl.colonia as ColoniaDomicilio
		,mo.municipio as MunicipioDomicilio
		,eo.estado as EstadoDomicilio
		,'' as MetodoConcepto
		,'' as CuentaTesoreria
		,'' as CuentaDictamen
FROM Detenido d
LEFT JOIN Detencion dt
ON d.id = dt.idDetenido
LEFT JOIN Municipio m
ON m.id = dt.idMunicipioDetencion
LEFT JOIN Estado e
ON e.id = m.idestado
LEFT JOIN Colonia cl
ON cl.id = d.idcolonia
LEFT JOIN Municipio mo
ON mo.id = d.idmunicipio
LEFT JOIN Estado eo
ON eo.id = mo.idestado

;